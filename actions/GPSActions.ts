export const SET_COORDS = "set_coords";

export function setCoordsToState(coords: Coordinates) {
	return dispatch => {
		dispatch({
			type: SET_COORDS,
			payload: coords
		});
	};
}
