import * as _ from "lodash";
import { fights, bombs, hqs, questions, players } from "../Firebase";

export const SET_FIGHT_INFO = "set_fight_info";

export function playerFightListener(uid: string) {
	console.log(uid);
	return dispatch => {
		fights.child(uid).on("value", data => {
			const obj = data.val();
			console.log(obj);
			dispatch({
				type: SET_FIGHT_INFO,
				payload: obj
			});
		});
	};
}

export const SET_BOMBS = "set_bombs";

export function bombsListener() {
	return dispatch => {
		bombs.on("value", data => {
			const obj = data.val();

			dispatch({
				type: SET_BOMBS,
				payload: _.map(obj, bomb => {
					return bomb;
				})
			});
		});
	};
}

export const SET_HQS = "set_hqs";

export function hqsListener() {
	return dispatch => {
		hqs.on("value", data => {
			const obj = data.val();
			console.log("hqs action");
			dispatch({
				type: SET_HQS,
				payload: _.map(obj, hq => {
					return hq;
				})
			});
		});
	};
}

export const SET_QUESTIONS = "set_questions";

export function questionsListener() {
	return dispatch => {
		questions.on("value", data => {
			const obj = data.val();
			dispatch({
				type: SET_QUESTIONS,
				payload: _.map(obj, question => {
					return question;
				})
			});
		});
	};
}

export const SET_PLAYERS = "set_players";

export function playersListener(team: number) {
	return dispatch => {
		const playersOnTeam = players.orderByChild("playerInfo/team").equalTo(team);

		playersOnTeam.on("value", data => {
			const obj = _.map(data.val(), (player, uid) => {
				return { ...player, uid: uid };
			});
			dispatch({
				type: SET_PLAYERS,
				payload: _.map(
					_.filter(obj, player => {
						return player.playerInfo.type === 1 ? true : false;
					}),
					player => {
						return player;
					}
				)
			});
		});
	};
}

export function playersOnce(team: number) {
	return dispatch => {
		const playersOnTeam = players.orderByChild("playerInfo/team").equalTo(team);

		playersOnTeam.once("value", data => {
			const obj = _.map(data.val(), (player, uid) => {
				return { ...player, uid: uid };
			});
			dispatch({
				type: SET_PLAYERS,
				payload: _.map(
					_.filter(obj, player => {
						return player.playerInfo.type === 1 ? true : false;
					}),
					player => {
						return player;
					}
				)
			});
		});
	};
}
