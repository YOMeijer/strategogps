import { teams } from "../Firebase.ts";

export const FETCH_TEAMS = "fetch_teams";

export function getTeams() {
  return dispatch => {
    teams.on("value", data => {
      console.log("teams");
      const obj = data.val();

      dispatch({
        type: FETCH_TEAMS,
        payload: obj
      });
    });
  };
}
