import { database, fights, game } from "../Firebase";
import * as _ from "lodash";

export const SET_DEVMODE = "set_devmode";

export function devModeListener() {
	return dispatch => {
		database
			.child("system")
			.child("devMode")
			.on("value", data => {
				const obj = data.val();
				dispatch({
					type: SET_DEVMODE,
					payload: obj
				});
			});
	};
}

export const SET_FIGHTS = "set_fights";

export function fightsListener() {
	return dispatch => {
		fights.on("value", data => {
			const obj = data.val();
			dispatch({
				type: SET_FIGHTS,
				payload: _.map(obj, (f, uid) => {
					return { ...f, uid: uid };
				})
			});
		});
	};
}

export const SET_GAME_INFO = "set_game_info";

export function gameListener() {
	return dispatch => {
		game.on("value", data => {
			const obj = data.val();
			console.log(obj);
			dispatch({
				type: SET_GAME_INFO,
				payload: obj
			});
		});
	};
}
