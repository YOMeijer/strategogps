import firebase from "firebase/app";
import "firebase/database";
import "firebase/auth";

var config = {
	apiKey: "AIzaSyD-ouxiQR-K7Jrd77P4gr8dMCdpcqj5icU",
	authDomain: "datatest-c2e1a.firebaseapp.com",
	databaseURL: "https://datatest-c2e1a.firebaseio.com",
	projectId: "datatest-c2e1a",
	storageBucket: "datatest-c2e1a.appspot.com",
	messagingSenderId: "393464665328",
	appId: "1:393464665328:web:570f1c60ae76d520"
};
firebase.initializeApp(config);

export const auth = firebase.auth();

export const database = firebase.database().ref();
export const players = database.child("players");
export const fights = database.child("fights");
export const bombs = database.child("bombs");
export const hqs = database.child("hqs");
export const killStream = database.child("killStream");
export const questions = database.child("questions");
export const game = database.child("game");

export interface UserInfo {
	uid: String;
	userType?: number;
}
