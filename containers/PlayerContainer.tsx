import * as React from "react";
import * as _ from "lodash";
import { connect } from "react-redux";
import { User } from "firebase";
import { PlayerState, FightState, PlayerDTO } from "../reducers/PlayerReducer";
import { playerFightListener, bombsListener, hqsListener } from "../actions/PlayerActions";
import { setCoordsToState } from "../actions/GPSActions";
import { fights, players, killStream, bombs } from "../Firebase";
import Spion from "../components/Ranks/Spion";
import Verkenner from "../components/Ranks/Verkenner";
import Mineur from "../components/Ranks/Mineur";
import { Bomb } from "../reducers/BombsReducer";
import Sergeant from "../components/Ranks/Sergeant";
import Luitenant from "../components/Ranks/Luitenant";
import "../styles/player-container.scss";
import DeathScreen from "../components/DeathScreen";
import AltRanks from "../components/Ranks/AltRanks";
import { distance, indexOfMin } from "../shared/shared.functions";
import Maarschalk from "../components/Ranks/Maarschalk";
import { AppStore } from "../reducers";
import { HQ } from "../reducers/HQsReducer";
import { GameState } from "../reducers/GameReducer";

type ThisState = {
	defendPopup: boolean;
	defendCode: number;
	fightCodeInvalid: boolean;
	fighting: boolean;
	isDead: boolean;
	isNearCircle: boolean;
	isInCircle: boolean;
	nameInput: string;
};

type ThisProps = {
	heading: number;
	coordsInput: Coordinates;

	playerFightListener(uid: string);
	bombsListener();
	hqsListener();
	setCoordsToState(coords: Coordinates);
	auth: User;
	player: PlayerState;
	fight: FightState;
	bombs: Bomb[];
	hqs: HQ[];
	game: GameState;
};

class PlayerContainer extends React.Component<ThisProps, ThisState> {
	componentWillMount() {
		this.setState({ defendPopup: false, fightCodeInvalid: false });
	}

	componentDidMount() {
		this.props.playerFightListener(this.props.auth.uid);
		this.props.bombsListener();
		this.props.hqsListener();
	}

	componentWillReceiveProps(nextProps: ThisProps) {
		if (nextProps.player) {
			console.log("isDead:", nextProps.player.isDead);
			this.setState({ isDead: nextProps.player.isDead });

			if (nextProps.player.isDead && nextProps.player.rank === 10) {
				const playersOnTeam = players.orderByChild("playerInfo/team").equalTo(nextProps.player.team);

				playersOnTeam.once("value", data => {
					const obj = _.map(data.val(), (player, uid) => {
						return { ...player, uid: uid };
					});
					const p = _.filter(
						_.orderBy(
							_.map(
								_.filter(obj, player => {
									return player.playerInfo.type === 1 ? true : false;
								}),
								player => {
									return player;
								}
							),
							function(pl: PlayerDTO) {
								return pl.playerInfo.rank;
							}
						),
						player => {
							return player.playerInfo.rank === 10 ? false : true;
						}
					);

					players
						.child(p[p.length - 1].uid)
						.child("playerInfo")
						.update({ rank: 10 });

					players
						.child(this.props.auth.uid)
						.child("playerInfo")
						.update({ rank: 2 });
				});
			}
		}

		if (nextProps.fight && !this.state.fighting) {
			this.setState({ fighting: true });
		} else if (this.state.fighting && !this.state.defendPopup) {
			this.setState({ fighting: false });
		}

		if (nextProps.coordsInput) {
			const newCoords: Coordinates = {
				accuracy: nextProps.coordsInput.accuracy ? nextProps.coordsInput.accuracy : null,
				altitude: nextProps.coordsInput.altitude ? nextProps.coordsInput.altitude : null,
				altitudeAccuracy: nextProps.coordsInput.altitudeAccuracy ? nextProps.coordsInput.altitudeAccuracy : null,
				heading: nextProps.heading ? nextProps.heading : this.props.heading,
				latitude: nextProps.coordsInput.latitude ? nextProps.coordsInput.latitude : null,
				longitude: nextProps.coordsInput.longitude ? nextProps.coordsInput.longitude : null,
				speed: nextProps.coordsInput.speed ? nextProps.coordsInput.speed : null
			};
			this.props.setCoordsToState(newCoords);

			// SET IN CIRCLE
			if (nextProps.hqs) {
				const distanceArray = _.map(nextProps.hqs, hq => {
					return parseInt(distance(hq.lat, hq.long, newCoords.latitude, newCoords.longitude));
				});

				if (distanceArray.length > 0) {
					const target = nextProps.hqs[indexOfMin(distanceArray)];
					const dist = parseInt(distance(newCoords.latitude, newCoords.longitude, target.lat, target.long));
					console.log(dist);
					if (dist < 11) {
						this.setState({ isInCircle: true, isNearCircle: true });
						const now = new Date().getTime();
						players.child(this.props.auth.uid).update({ lastHQTime: now, hqId: target.id });
					} else if (dist < 25) {
						this.setState({ isInCircle: false, isNearCircle: true });
					} else {
						this.setState({ isInCircle: false, isNearCircle: false });
					}
				}
			}

			// DIE ON BOMB

			if (this.props.bombs && this.props.player && this.props.player.rank !== 3) {
				const filteredBombs: Bomb[] = _.filter(nextProps.bombs, bomb => {
					if (this.props.player.team !== bomb.team) {
						return true;
					}
				});
				const distanceArray = _.map(filteredBombs, bomb => {
					return parseInt(distance(bomb.lat, bomb.long, newCoords.latitude, newCoords.longitude));
				});
				if (distanceArray.length > 0) {
					const bombIndex = indexOfMin(distanceArray);
					const closeBomb = distanceArray[bombIndex];
					if (closeBomb < 11) {
						if (!nextProps.player.isDead && !this.props.player.isDead) {
							const now = new Date().getTime();
							players
								.child(this.props.auth.uid)
								.child("playerInfo")
								.update({
									isDead: true,
									killedBy: filteredBombs[bombIndex].name,
									killedByRank: 0,
									timeOfDeath: now,
									reviveCode: Math.floor(Math.random() * 899 + 100)
								});
							// killStream.child(now.toString()).set({
							// 	killer: filteredBombs[bombIndex].name,
							// 	killerRank: 0,
							// 	killerTeam: filteredBombs[bombIndex].team,
							// 	victim: this.props.player.name,
							// 	victimRank: this.props.player.rank,
							// 	victimTeam: this.props.player.team
							// });
							bombs.child(filteredBombs[bombIndex].id).remove();
						}
					}
				}
			}
		}
	}

	attack() {
		const fight: FightState = {
			id: Math.floor(Math.random() * 899 + 100),
			attackTeam: this.props.player.team,
			attackRank: this.props.player.rank,
			attackName: this.props.player.name
		};
		fights.child(this.props.auth.uid).set(fight);
		this.setState({ fighting: true });
	}

	cancelAttack() {
		fights.child(this.props.auth.uid).remove();
		this.setState({ fighting: false });
	}

	startDefend() {
		this.setState({
			defendPopup: true,
			fightCodeInvalid: false,
			defendCode: null,
			fighting: true
		});
	}

	cancelDefend() {
		this.setState({
			defendPopup: false,
			fightCodeInvalid: false,
			defendCode: null,
			fighting: false
		});
	}

	defend() {
		const fightRef = fights.orderByChild("id").equalTo(this.state.defendCode);
		fightRef.once("value", snapshot => {
			if (!snapshot.val()) {
				this.setState({ fightCodeInvalid: true });
			} else {
				const fight: FightState = _.map(snapshot.val(), (fight, uid) => {
					return { ...fight, uid: uid };
				})[0];
				if (fight.attackTeam === this.props.player.team) {
					this.setState({ fightCodeInvalid: true });
				} else {
					this.setState({ fightCodeInvalid: false, fighting: true });
					fights.child(fight.uid).update({
						defendTeam: this.props.player.team,
						defendRank: this.props.player.rank,
						defendName: this.props.player.name,
						defendUid: this.props.auth.uid
					});
				}
			}
		});
	}

	handleDefendInput(i: string) {
		this.setState({ defendCode: parseInt(i) });
	}

	// renderBombs() {
	// 	if (this.props.bombs) {
	// 		return _.map(this.props.bombs, (bomb, index) => {
	// 			return (
	// 				<div key={"bomb" + index}>
	// 					<div>bomb:</div>
	// 					<div>team: {bomb.team}</div>
	// 				</div>
	// 			);
	// 		});
	// 	} else {
	// 		return null;
	// 	}
	// }

	handleNameInput(name: string) {
		this.setState({ nameInput: name });
	}

	saveName() {
		players
			.child(this.props.auth.uid)
			.child("playerInfo")
			.update({ name: this.state.nameInput });
	}

	selectTeam(team: number) {
		players
			.child(this.props.auth.uid)
			.child("playerInfo")
			.update({ team: team });
	}

	render() {
		return (
			<div className="player-container">
				{this.props.game && this.props.game.startingGame ? this.props.auth.uid : null}
				{this.props.player && this.props.player.name === "Nieuwe speler" && this.props.game.startingGame ? (
					<div className="flx-col">
						<br />
						<label>Naam:</label>
						<input type="text" onChange={event => this.handleNameInput(event.target.value)} />
						<br />
						<button className="btn-reg" onClick={() => this.saveName()}>
							Opslaan
						</button>
					</div>
				) : (
					<div>
						{this.props.player && this.props.player.team === 0 && this.props.game.startingGame ? (
							<div className="flx-col">
								<div>Selecteer team:</div>
								<br />
								<button className="btn-reg" onClick={() => this.selectTeam(1)}>
									Team blauw
								</button>
								<br />
								<button className="btn-reg" onClick={() => this.selectTeam(2)}>
									Team rood
								</button>
							</div>
						) : (
							<div>
								{this.props.player && this.props.player.team !== 0 && !this.props.game.gameEnd ? (
									<div>
										<div>
											{this.props.player && this.props.player.isDead ? (
												<div style={{ minHeight: "75vh" }}>
													<DeathScreen />
												</div>
											) : null}
											{this.props.player && !this.props.player.isDead && !this.state.fighting ? (
												<div style={{ minHeight: "75vh" }}>
													{(() => {
														switch (this.props.player.rank) {
															case 1:
																return <Spion />;
															case 2:
																return <Verkenner />;
															case 3:
																return <Mineur isNearCircle={this.state.isNearCircle} />;
															case 4:
																return <Sergeant isNearCircle={this.state.isNearCircle} />;
															case 5:
																return <Luitenant />;
															case 6:
															case 7:
															case 8:
															case 9:
																return <AltRanks isNearCircle={this.state.isNearCircle} />;
															case 10:
																return <Maarschalk isNearCircle={this.state.isNearCircle} />;
															default:
																return "no rank";
														}
													})()}{" "}
												</div>
											) : null}
										</div>
										{this.props.player &&
										!this.props.player.isDead &&
										this.props.game &&
										!this.props.game.startingGame ? (
											<div className="flx-col" style={{ minHeight: "15vh" }}>
												{this.state.fighting ? (
													<div style={{ minHeight: "75vh" }}>
														{this.props.fight ? (
															<div>
																<button
																	className="btn-close"
																	onClick={() => {
																		this.cancelAttack();
																	}}>
																	X
																</button>
																<br />
																<br />
																<div className="lg-text flx-col">
																	<strong>Code:</strong>
																	<strong style={{ fontSize: "10vmin" }}>{this.props.fight.id}</strong>
																</div>
															</div>
														) : null}

														{this.state.defendPopup ? (
															<div>
																<button
																	className="btn-close"
																	onClick={() => {
																		this.cancelDefend();
																	}}>
																	X
																</button>
																<div className="flx-col-wide" style={{ minHeight: "75vh" }}>
																	{this.state.fightCodeInvalid ? (
																		<div className="lg-text">Code is ongeldig...</div>
																	) : null}
																	<input
																		className="game-input"
																		autoFocus
																		type="number"
																		onChange={event => this.handleDefendInput(event.target.value)}
																	/>
																	<button
																		className="btn-reg"
																		onClick={() => {
																			this.defend();
																		}}>
																		Verdedigen
																	</button>
																</div>
															</div>
														) : null}
													</div>
												) : null}
												{!this.state.fighting ? (
													<div className="flx-row">
														<button
															className="btn-reg"
															onClick={() => {
																this.startDefend();
															}}>
															Verdedigen
														</button>
														<div style={{ width: "2vmin" }} />
														<button
															className="btn-reg"
															onClick={() => {
																this.attack();
															}}>
															Aanvallen
														</button>
													</div>
												) : null}
											</div>
										) : null}
									</div>
								) : (
									<div className="flx-row" style={{ minHeight: "75vh" }}>
										{this.props.game.gameEnd ? "Spel is afgelopen" : "Loading..."}
									</div>
								)}
							</div>
						)}
					</div>
				)}
			</div>
		);
	}
}

function mapStateToProps(state: AppStore) {
	return {
		auth: state.auth,
		player: state.player,
		fight: state.fight,
		bombs: state.bombs,
		hqs: state.hqs,
		game: state.game
	};
}

export default connect(
	mapStateToProps,
	{ playerFightListener, bombsListener, setCoordsToState, hqsListener }
)(PlayerContainer);
