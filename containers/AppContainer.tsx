import * as React from "react";
import { Suspense, lazy, Component } from "react";
import { Switch, Route } from "react-router-dom";
import { applyMiddleware, createStore } from "redux";
import thunk from "redux-thunk";
import reducers from "../reducers";
import { Provider } from "react-redux";

const createStoreWithMiddleWare = applyMiddleware(thunk)(createStore);

const AuthContainer = lazy(() => import("./AuthContainer"));

export default class AppContainer extends Component {
	render() {
		return (
			<Provider store={createStoreWithMiddleWare(reducers)}>
				<Suspense fallback={<div>Loading...</div>}>
					<Switch>
						<Route path="/" component={AuthContainer} />
					</Switch>
				</Suspense>
			</Provider>
		);
	}
}
