import * as React from "react";
import * as _ from "lodash";
import { connect } from "react-redux";
import { PlayerDTO, PlayerState } from "../reducers/PlayerReducer";
import { playersListener } from "../actions/PlayerActions";
import { AppStore } from "../reducers";
import RankIcon from "../components/RankIcon";
import { players, game } from "../Firebase";
import Score from "../components/Score";
import { GameState } from "../reducers/GameReducer";

type ThisState = {
	interval: any;
	time: number;
};

type ThisProps = {
	playersListener(team: number);
	players: PlayerDTO[];
	player: PlayerState;
	game: GameState;
};

class BaseContainer extends React.Component<ThisProps, ThisState> {
	componentWillMount() {
		this.setState({
			interval: setInterval(() => {
				this.setState({ time: new Date().getTime() });
			}, 1000)
		});
	}

	componentWillReceiveProps(nextProps: ThisProps) {
		if (nextProps.player && !nextProps.players) {
			this.props.playersListener(nextProps.player.team);
		}
	}

	componentWillUnmount() {
		clearInterval(this.state.interval);
		this.setState({ interval: null });
	}

	renderDeath() {
		return (
			<svg
				xmlns="http://www.w3.org/2000/svg"
				viewBox="0 0 448 512"
				style={{ height: "40px", width: "40px", margin: "0px 1%" }}>
				<path
					fill="white"
					d="M439.15 453.06L297.17 384l141.99-69.06c7.9-3.95 11.11-13.56 7.15-21.46L432 264.85c-3.95-7.9-13.56-11.11-21.47-7.16L224 348.41 37.47 257.69c-7.9-3.95-17.51-.75-21.47 7.16L1.69 293.48c-3.95 7.9-.75 17.51 7.15 21.46L150.83 384 8.85 453.06c-7.9 3.95-11.11 13.56-7.15 21.47l14.31 28.63c3.95 7.9 13.56 11.11 21.47 7.15L224 419.59l186.53 90.72c7.9 3.95 17.51.75 21.47-7.15l14.31-28.63c3.95-7.91.74-17.52-7.16-21.47zM150 237.28l-5.48 25.87c-2.67 12.62 5.42 24.85 16.45 24.85h126.08c11.03 0 19.12-12.23 16.45-24.85l-5.5-25.87c41.78-22.41 70-62.75 70-109.28C368 57.31 303.53 0 224 0S80 57.31 80 128c0 46.53 28.22 86.87 70 109.28zM280 112c17.65 0 32 14.35 32 32s-14.35 32-32 32-32-14.35-32-32 14.35-32 32-32zm-112 0c17.65 0 32 14.35 32 32s-14.35 32-32 32-32-14.35-32-32 14.35-32 32-32z"
				/>
			</svg>
		);
	}

	revivePlayer(uid, rank: number) {
		players
			.child(uid)
			.child("playerInfo")
			.update({ isDead: false });

		game
			.child("score")
			.child("team" + this.props.player.team)
			.set(this.props.game.score["team" + this.props.player.team] - (40 + rank * 10));
	}

	renderTeam() {
		if (this.props.players) {
			return _.map(
				_.orderBy(this.props.players, player => {
					return player.playerInfo.rank;
				}),
				player => {
					return (
						<div
							className={"flx-row-wide base-player-row " + (player.playerInfo.isDead ? "is-dead" : "")}
							key={player.playerInfo.name}>
							<div className="flx-row-left">
								{player.playerInfo.isDead ? (
									this.renderDeath()
								) : (
									<RankIcon rank={player.playerInfo.rank} size={"50px"} />
								)}
								<div>{player.playerInfo.name}</div>
							</div>
							{player.playerInfo.isDead ? (
								<button
									className="revive-button"
									disabled={this.state.time - player.playerInfo.timeOfDeath < 1000 * 90}
									onClick={() => this.revivePlayer(player.uid, player.playerInfo.rank)}>
									<svg
										style={{ height: "30px", width: "30px" }}
										xmlns="http://www.w3.org/2000/svg"
										viewBox="0 0 448 512">
										<path
											fill="green"
											d="M400 32H48C21.5 32 0 53.5 0 80v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V80c0-26.5-21.5-48-48-48zm-32 252c0 6.6-5.4 12-12 12h-92v92c0 6.6-5.4 12-12 12h-56c-6.6 0-12-5.4-12-12v-92H92c-6.6 0-12-5.4-12-12v-56c0-6.6 5.4-12 12-12h92v-92c0-6.6 5.4-12 12-12h56c6.6 0 12 5.4 12 12v92h92c6.6 0 12 5.4 12 12v56z"
										/>
									</svg>
								</button>
							) : null}
						</div>
					);
				}
			).reverse();
		} else {
			return null;
		}
	}

	buyBomb() {
		game
			.child("bombs")
			.child("team" + this.props.player.team)
			.set(this.props.game.bombs["team" + this.props.player.team] + 1);
		game
			.child("score")
			.child("team" + this.props.player.team)
			.set(this.props.game.score["team" + this.props.player.team] - 25);
	}

	render() {
		return (
			<div>
				{this.props.player ? (
					<div className={"base-container team" + (this.props.player.team ? this.props.player.team.toString() : "0")}>
						{this.props.game ? (
							<div className="lg-text flx-row">
								Opdrachten/vragen: {this.props.game.questions["team" + this.props.player.team]}
							</div>
						) : null}
						{this.props.game ? (
							<div className="flx-row">
								<button
									className="btn-reg"
									onClick={() => this.buyBomb()}
									disabled={this.props.game.bombs["team" + this.props.player.team] >= 2}>
									Koop bom
								</button>
								{this.props.game.bombs["team" + this.props.player.team] >= 1 ? (
									<RankIcon rank={0} size={"10vmin"} />
								) : null}
								{this.props.game.bombs["team" + this.props.player.team] >= 2 ? (
									<RankIcon rank={0} size={"10vmin"} />
								) : null}
							</div>
						) : null}
						<Score />
						{this.renderTeam()}
					</div>
				) : (
					<div>Loading...</div>
				)}
			</div>
		);
	}
}

function mapStateToProps(state: AppStore) {
	return {
		players: state.players,
		player: state.player,
		game: state.game
	};
}

export default connect(
	mapStateToProps,
	{ playersListener }
)(BaseContainer);
