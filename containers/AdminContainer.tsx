import * as React from "react";
import * as _ from "lodash";
import { connect } from "react-redux";
import { Switch, Route, NavLink } from "react-router-dom";
import MarkHQ from "../components/MarkHQ";
import { hqs, questions, game } from "../Firebase";
import { randomString } from "../shared/shared.functions";
import { HQ } from "../reducers/HQsReducer";
import MarkQuestion from "../components/MarkQuestion";
import { Question } from "../reducers/QuestionsReducer";
import { AppStore } from "../reducers";
import { questionsListener, hqsListener } from "../actions/PlayerActions";
import { GameState } from "../reducers/GameReducer";
import Score from "../components/Score";

type ThisState = {
	boo: string;
};

type ThisProps = {
	location: string;
	coordsInput: Coordinates;
	hqs: HQ[];
	questions: Question[];
	game: GameState;
	hqsListener();
	questionsListener();
};

class AdminContainer extends React.Component<ThisProps, ThisState> {
	componentWillMount() {
		this.setState({ boo: "boo" });
		this.props.hqsListener();
		this.props.questionsListener();
	}

	componentWillReceiveProps(nextProps) {
		// if(nextProps.user.user === null && nextProps.user.loading === false) {
		//   this.props.history.replace('/Login');
		// }
	}

	markHQ() {
		const newHQ: HQ = {
			lat: this.props.coordsInput.latitude,
			long: this.props.coordsInput.longitude,
			id: randomString(10),
			active: false
		};
		hqs.child(newHQ.id).set(newHQ);
	}

	markQuestion(team: number) {
		const newQuestion: Question = {
			lat: this.props.coordsInput.latitude,
			long: this.props.coordsInput.longitude,
			id: randomString(10),
			team: team
		};
		questions.child(newQuestion.id).set(newQuestion);
	}

	add100(team: number) {
		game
			.child("score")
			.child("team" + team)
			.set(this.props.game.score["team" + team] + 100);
	}

	render() {
		return (
			<div>
				<React.Suspense fallback={<div>Loading...</div>}>
					<div>ADMIN</div>
					<Score />
					{this.props.coordsInput ? (
						<div>
							<div>Accuracy: {parseInt(this.props.coordsInput.accuracy.toString())}m</div>
							<div>{this.props.coordsInput.latitude}</div>
							<div>{this.props.coordsInput.longitude}</div>
						</div>
					) : null}
					<br />
					<div>Aantal vlaggen: {this.props.hqs ? this.props.hqs.length : "onbekend"}</div>
					<div>Aantal opdrachten: {this.props.questions ? this.props.questions.length : "onbekend"}</div>
					<br />
					<MarkHQ onMarkHq={() => this.markHQ()} />
					<br />
					<MarkQuestion onMarkQuestion={(team: number) => this.markQuestion(team)} />
					<br />
					<div className="flx-row">
						<button className="btn-reg text-blauw" onClick={() => this.add100(1)}>
							+100
						</button>
						<button className="btn-reg text-rood" onClick={() => this.add100(2)}>
							+100
						</button>
					</div>
				</React.Suspense>
			</div>
		);
	}
}

function mapStateToProps(state: AppStore) {
	return {
		coords: state.coords,
		hqs: state.hqs,
		questions: state.questions,
		game: state.game
	};
}

export default connect(
	mapStateToProps,
	{ questionsListener, hqsListener }
)(AdminContainer);
