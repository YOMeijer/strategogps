import React, { Component } from "react";
import SimpleBox from "../components/oldComponents/SimpleBox";
import InputField from "../components/oldComponents/InputField";
import FooterFormButton from "../components/oldComponents/FooterFormButton";
import { login, getUser, getUserInfo } from "../actions/UserActions";
import { connect } from "react-redux";
import ErrorAlert from "../components/oldComponents/ErrorAlert";

class Login extends Component {
	constructor(props) {
		super(props);
		this.state = {
			email: "",
			password: "",
			error: ""
		};
	}

	componentWillMount() {
		this.props.getUser();
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.user.userInfo !== null) {
			this.props.history.push("/");
		}
	}

	submitLogin(event) {
		event.preventDefault();
		this.props.login(this.state.email, this.state.password);
	}

	translateErr(err) {
		if (err === "The email address is badly formatted.") {
			return "Email adres bestaat niet";
		} else if (err === "There is no user record corresponding to this identifier. The user may have been deleted.") {
			return "Er is geen account geregistreerd met dit email adres.";
		} else if (err === "The password is invalid or the user does not have a password.") {
			return "Wachtwoord onjuist";
		} else {
			return err;
		}
	}

	renderBody() {
		const errStyle = {
			borderColor: "red"
		};

		return (
			<form
				onSubmit={event => {
					this.submitLogin(event);
				}}>
				<div>
					<InputField
						id="email"
						type="text"
						label="Email"
						inputAction={event => this.setState({ email: event.target.value })}
						style={this.props.loginErr ? errStyle : null}
					/>
					<InputField
						id="password"
						type="password"
						label="Wachtwoord"
						inputAction={event => this.setState({ password: event.target.value })}
						style={this.props.loginErr ? errStyle : null}
					/>
					{this.props.loginErr && <ErrorAlert>{this.translateErr(this.props.loginErr.message)}</ErrorAlert>}
					<FooterFormButton submitLabel="Login" otherLabel="Nieuw Account" goToLink="/Signup" {...this.props} />
				</div>
			</form>
		);
	}

	render() {
		document.getElementById("page-title").innerHTML = "Log in";
		return (
			<div className="inner-body">
				<div className="app-title">Planning</div>
				<div className="app-sub-title">(Beta)</div>
				<SimpleBox title="Login" body={this.renderBody()} />
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		user: state.user,
		loginErr: state.loginErr
	};
}

export default connect(
	mapStateToProps,
	{ login, getUser, getUserInfo }
)(Login);
