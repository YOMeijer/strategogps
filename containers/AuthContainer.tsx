import * as React from "react";
import { Suspense, lazy, Component } from "react";
import { Switch, Route, NavLink } from "react-router-dom";
import { playerInfoListener, authState } from "../actions/UserActions";
import { devModeListener, gameListener } from "../actions/SystemActions";
import { connect } from "react-redux";
import { User } from "firebase";
import { auth, players } from "../Firebase";
import { PlayerState } from "../reducers/PlayerReducer";
import { SystemState } from "../reducers/SystemReducer";
import "../styles/app-container.scss";
import { GameState } from "../reducers/GameReducer";

const GPSContainer = lazy(() => import("./GPSContainer"));
const BaseContainer = lazy(() => import("./BaseContainer"));
const SystemContainer = lazy(() => import("./SystemContainer"));
const ResetContainer = lazy(() => import("./ResetContainer"));

type AuthContainerState = {
	uid?: string;
	email?: string;
	newPlayerError?: string;
};

type ThisProps = {
	playerInfoListener(uid: string);
	authState();
	devModeListener();
	gameListener();
	auth: User;
	player: PlayerState;
	system: SystemState;
	history: any;
	match: any;
	location: any;
	game: GameState;
};

export class AuthContainer extends Component<ThisProps, AuthContainerState> {
	componentWillMount() {
		this.setState({ uid: null });

		this.props.authState();
		this.props.devModeListener();
		this.props.gameListener();
	}

	componentWillReceiveProps(nextProps: ThisProps) {
		if (nextProps.player) {
			setTimeout(() => {
				switch (nextProps.player.type) {
					case 0:
						if (!nextProps.location.pathname.includes("/admin")) {
							this.props.history.push("/admin");
						}
						break;
					case 1:
						if (nextProps.location.pathname !== "/player") {
							this.props.history.push("/player");
						}
						break;
					case 2:
						if (nextProps.location.pathname !== "/base") {
							this.props.history.push("/base");
						}
						break;
					case 3:
						if (!nextProps.location.pathname.includes("/system")) {
							this.props.history.push("/system");
						}
						break;
					default:
						break;
				}
			}, 500);
		}

		if (nextProps.auth) {
			if (nextProps.auth.uid && !this.state.uid) {
				this.setState({ uid: nextProps.auth.uid });
				this.props.playerInfoListener(nextProps.auth.uid);
			}
		}
	}

	switchUser(i: number) {
		auth.signOut();
		switch (i) {
			case 0:
				this.signIn("yurimeijer@xs4all.nl", "tttttt");
				break;
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
				this.signIn(`yurimeijer${i}@xs4all.nl`, "tttttt");
				break;

			case 6:
				this.signIn("base2@base.com", "tttttt");

			default:
				break;
		}
	}

	switchRank(i: number) {
		players
			.child(this.props.auth.uid)
			.child("playerInfo")
			.child("rank")
			.set(i);
	}

	die() {
		players
			.child(this.props.auth.uid)
			.child("playerInfo")
			.update({
				isDead: true,
				reviveCode: Math.floor(Math.random() * 899 + 100),
				timeOfDeath: new Date().getTime()
			});
	}

	signIn(email: string, pass: string) {
		auth
			.signInWithEmailAndPassword(email, pass)
			.then(res => {
				window.location.reload(true);
			})
			.catch(function(error) {
				// Handle Errors here.
				var errorCode = error.code;
				var errorMessage = error.message;

				console.error(errorCode);
				console.error(errorMessage);
				// ...
			});
	}

	handleEmailInput(email) {
		this.setState({ email: email, newPlayerError: null });
	}

	newPlayer() {
		auth.createUserWithEmailAndPassword(this.state.email, "tttttt").catch(
			function(error) {
				// Handle Errors here.
				var errorCode = error.code;
				var errorMessage = error.message;

				console.error(errorCode);
				console.error(errorMessage);

				this.setState({ newPlayerError: errorMessage });
				console.log(this.state);
			}.bind(this)
		);
	}

	render() {
		return (
			<Suspense fallback={<div>Loading...</div>}>
				<div className={"app-container team" + (this.props.player ? this.props.player.team : "")}>
					{this.props.system.devMode ? (
						<div>
							<i>DEVMODE</i>
							<br />
							<div>
								<button onClick={() => this.switchUser(0)}>Player 1</button>
								<button onClick={() => this.switchUser(1)}>Player 2</button>
								<button onClick={() => this.switchUser(5)}>Player 3</button>
								<button onClick={() => this.switchUser(2)}>Admin</button>
								<button onClick={() => this.switchUser(3)}>Base blauw</button>
								<button onClick={() => this.switchUser(6)}>Base rood</button>
								<button onClick={() => this.switchUser(4)}>System</button>
							</div>
							<br />
							<div>
								<button onClick={() => this.switchRank(1)}>1</button>
								<button onClick={() => this.switchRank(2)}>2</button>
								<button onClick={() => this.switchRank(3)}>3</button>
								<button onClick={() => this.switchRank(4)}>4</button>
								<button onClick={() => this.switchRank(5)}>5</button>
								<button onClick={() => this.switchRank(9)}>9</button>
								<button onClick={() => this.switchRank(10)}>10</button>
							</div>
							<div>
								<button onClick={() => this.die()}>Dead</button>
							</div>
							{this.props.player
								? (() => {
										switch (this.props.player.type) {
											case 0:
												return "user is admin";
											case 1:
												return "user is player";
											case 2:
												return "user is base";
											case 3:
												return "user is system";
											default:
												return "user is loading...";
										}
								  })()
								: "user is loading..."}

							{this.props.auth ? (
								<div>
									<br />
									<div>{this.props.auth.uid}</div>
									<div>{this.props.auth.email}</div>{" "}
								</div>
							) : null}
						</div>
					) : null}
					<br />
					{this.props.auth && this.props.auth.uid ? (
						<div>
							{!this.props.game.startingGame ? (
								<NavLink exact activeClassName="active" to={"/reset"}>
									<div className="flx-row refresh-gps lg-text">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
											<path
												fill="white"
												d="M370.72 133.28C339.458 104.008 298.888 87.962 255.848 88c-77.458.068-144.328 53.178-162.791 126.85-1.344 5.363-6.122 9.15-11.651 9.15H24.103c-7.498 0-13.194-6.807-11.807-14.176C33.933 94.924 134.813 8 256 8c66.448 0 126.791 26.136 171.315 68.685L463.03 40.97C478.149 25.851 504 36.559 504 57.941V192c0 13.255-10.745 24-24 24H345.941c-21.382 0-32.09-25.851-16.971-40.971l41.75-41.749zM32 296h134.059c21.382 0 32.09 25.851 16.971 40.971l-41.75 41.75c31.262 29.273 71.835 45.319 114.876 45.28 77.418-.07 144.315-53.144 162.787-126.849 1.344-5.363 6.122-9.15 11.651-9.15h57.304c7.498 0 13.194 6.807 11.807 14.176C478.067 417.076 377.187 504 256 504c-66.448 0-126.791-26.136-171.315-68.685L48.97 471.03C33.851 486.149 8 475.441 8 454.059V320c0-13.255 10.745-24 24-24z"
											/>
										</svg>
										<div>Reset GPS</div>
									</div>
								</NavLink>
							) : null}
							<Switch>
								<Route
									path="/admin"
									render={props => <GPSContainer {...props} auth={this.props.auth} isPlayer={false} />}
								/>
								<Route
									path="/player"
									render={props => (
										<GPSContainer
											{...props}
											auth={this.props.auth}
											isPlayer={true}
											isDead={this.props.player ? this.props.player.isDead : true}
										/>
									)}
								/>
								<Route path="/base" component={BaseContainer} />
								<Route path="/system" component={SystemContainer} />
								<Route path="/reset" component={ResetContainer} />
							</Switch>
						</div>
					) : (
						<div className="flx-col">
							<div>{this.state.newPlayerError ? "Er is iets fout gegaan..." : null}</div>
							<br />
							<strong>Email:</strong>
							<input autoFocus type="email" onChange={event => this.handleEmailInput(event.target.value)} />
							<br />
							<button className="btn-reg" onClick={() => this.newPlayer()}>
								Start
							</button>
						</div>
					)}
				</div>
			</Suspense>
		);
	}
}

function mapStateToProps(state) {
	return {
		auth: state.auth,
		player: state.player,
		system: state.system,
		game: state.game
	};
}

export default connect(
	mapStateToProps,
	{ authState, playerInfoListener, devModeListener, gameListener }
)(AuthContainer);
