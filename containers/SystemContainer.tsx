import * as React from "react";
import * as _ from "lodash";
import { connect } from "react-redux";
import { players, fights, killStream, game, hqs } from "../Firebase";
import { fightsListener } from "../actions/SystemActions";
import { SystemState } from "../reducers/SystemReducer";
import { GameState } from "../reducers/GameReducer";
import { PlayerDTO } from "../reducers/PlayerReducer";
import { hqsListener } from "../actions/PlayerActions";
import { HQ } from "../reducers/HQsReducer";

type ThisState = {
	scoreInterval: any;
};

type ThisProps = {
	location: string;
	system: SystemState;
	game: GameState;
	fightsListener();
	hqsListener();
	hqs: HQ[];
};

class SystemContainer extends React.Component<ThisProps, ThisState> {
	componentWillMount() {
		this.setState({
			scoreInterval: setInterval(() => {
				this.checkHQ();
			}, 10000)
		});
		this.props.fightsListener();
		this.props.hqsListener();
	}

	componentWillReceiveProps(nextProps: ThisProps) {
		if (nextProps.system && nextProps.system.fights.length > 0) {
			const filteredFights = _.filter(nextProps.system.fights, fight => {
				return fight.defendUid ? true : false;
			});
			if (filteredFights.length > 0) {
				const fightToHandle = filteredFights[0];
				let winner = "draw";
				if (fightToHandle.attackRank === fightToHandle.defendRank) {
					winner = Math.random() > 0.5 ? "defend" : "attack";
				} else if (fightToHandle.attackRank === 1 && fightToHandle.defendRank === 10) {
					winner = "attack";
				} else if (fightToHandle.attackRank === 10 && fightToHandle.defendRank === 1) {
					winner = "defend";
				} else if (fightToHandle.attackRank > fightToHandle.defendRank) {
					winner = "attack";
				} else if (fightToHandle.attackRank < fightToHandle.defendRank) {
					winner = "defend";
				}

				const now = new Date().getTime();

				// killStream.child(now.toString()).set({
				// 	killer: winner === "attack" ? fightToHandle.attackName : fightToHandle.defendName,
				// 	killerRank: winner === "attack" ? fightToHandle.attackRank : fightToHandle.defendRank,
				// 	killerTeam: winner === "attack" ? fightToHandle.attackTeam : fightToHandle.defendTeam,
				// 	victim: winner === "attack" ? fightToHandle.defendName : fightToHandle.attackName,
				// 	victimRank: winner === "attack" ? fightToHandle.defendRank : fightToHandle.attackRank,
				// 	victimTeam: winner === "attack" ? fightToHandle.defendTeam : fightToHandle.attackTeam
				// });

				players
					.child(winner === "attack" ? fightToHandle.defendUid : fightToHandle.uid)
					.child("playerInfo")
					.update({
						isDead: true,
						killedBy: winner === "attack" ? fightToHandle.attackName : fightToHandle.defendName,
						killedByRank: winner === "attack" ? fightToHandle.attackRank : fightToHandle.defendRank,
						timeOfDeath: now,
						reviveCode: Math.floor(Math.random() * 899 + 100)
					});

				fights.child(fightToHandle.uid).remove();

				console.log("Fight finished...", fightToHandle.attackName, "VS", fightToHandle.defendName);
			}
		}
	}

	componentWillUnmount() {
		clearInterval(this.state.scoreInterval);
	}

	checkHQ() {
		console.log("checkHQ");
		const now = new Date().getTime();
		const playersinCircle = players.orderByChild("lastHQTime").startAt(now - 10000);
		if (this.props.hqs && this.props.hqs.length > 0) {
			playersinCircle.once("value", snapshot => {
				const players: PlayerDTO[] = _.map(snapshot.val(), player => player);

				const activeHQs = _.filter(this.props.hqs, hq => {
					return hq.active;
				});

				const activeHQid = activeHQs[0] ? activeHQs[0].id : null;

				if (players.length > 0) {
					if (!activeHQid) {
						hqs.child(players[0].hqId).update({ active: true, startTime: now });
					} else {
						_.filter(players, player => {
							return player.hqId === activeHQid ? false : true;
						});
					}

					const team1: PlayerDTO[] = _.filter(players, player => {
						if (player.playerInfo) {
							return player.playerInfo.team === 1 ? true : false;
						} else {
							return false;
						}
					});

					const team2: PlayerDTO[] = _.filter(players, player => {
						if (player.playerInfo) {
							return player.playerInfo.team === 2 ? true : false;
						} else {
							return false;
						}
					});

					if (team1.length > 0 && team2.length > 0) {
						game.update({ hqContested: true });
						console.log("hq contested");
					} else if (team1.length > 0) {
						let score = 0;
						team1.forEach(player => {
							score = score + 10 + player.playerInfo.rank;
						});
						game.child("score").update({ team1: this.props.game.score.team1 + score });
						game.update({ hqContested: false });
						console.log("team 1 in circle", score);
					} else if (team2.length > 0) {
						let score = 0;
						team2.forEach(player => {
							score = score + 10 + player.playerInfo.rank;
						});
						game.child("score").update({ team2: this.props.game.score.team2 + score });
						game.update({ hqContested: false });
						console.log("team 2 in circle", score);
					}
					console.log(team1, team2);
					console.log(this.props.game.score);
				} else {
					game.update({ hqContested: false });
				}

				if (activeHQid && activeHQs[0].active && now - activeHQs[0].startTime > 1000 * 60 * 5) {
					hqs.child(activeHQid).remove();
				}
			});
		}

		// players
		//   .child("I3pfw21cIch2sP5cBdNnnHNm1f22")
		//   .update({ lastHQTime: now + 1000 });
		// players
		//   .child("veb0GNzoSIUNm1o03Y4J4cqA7CB3")
		//   .update({ lastHQTime: now + 1000 });
	}

	render() {
		return (
			<div>
				<div>SYSTEM</div>
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		system: state.system,
		game: state.game,
		hqs: state.hqs
	};
}

export default connect(
	mapStateToProps,
	{ fightsListener, hqsListener }
)(SystemContainer);
