import * as React from "react";

type ThisState = {};

type ThisProps = {};

export default class ResetContainer extends React.Component<ThisProps, ThisState> {
	componentWillMount() {}

	render() {
		return <div className="base-container">RESETTING..</div>;
	}
}
