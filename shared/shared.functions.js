export function randomString(length) {
	var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz".split("");

	if (!length) {
		length = Math.floor(Math.random() * chars.length);
	}

	var str = "";
	for (var i = 0; i < length; i++) {
		str += chars[Math.floor(Math.random() * chars.length)];
	}
	return str;
}

export function dateDDMM(ms) {
	const d = new Date(+ms);
	return d.getDate() + "/" + (d.getMonth() + 1);
}

export function timeHHmm(ms) {
	const d = new Date(+ms);
	return d.getHours() + ":" + pwz(d.getMinutes(), 2);
}

export function msToTime(duration) {
	let seconds = Math.floor((duration / 1000) % 60),
		minutes = Math.floor((duration / (1000 * 60)) % 60),
		hours = Math.floor((duration / (1000 * 60 * 60)) % 24);

	hours = hours < 10 ? "0" + hours : hours;
	minutes = minutes < 10 ? "0" + minutes : minutes;
	seconds = seconds < 10 ? "0" + seconds : seconds;

	return (hours > 0 ? hours + ":" : "") + minutes + ":" + seconds;
}

export function datePlaceholder(ms) {
	let d;
	if (ms === "today") {
		d = new Date();
	} else {
		d = new Date(+ms);
	}
	return d.getFullYear() + "-" + pwz(d.getMonth() + 1, 2) + "-" + pwz(d.getDate(), 2);
}

export function pwz(number, length) {
	var my_string = "" + number;
	while (my_string.length < length) {
		my_string = "0" + my_string;
	}

	return my_string;
}

export function nameToAvatar(name) {
	const noVowels = name.replace(/[aeiou]/gi, "").split("");
	let my_string;
	if (noVowels.length >= 2) {
		my_string = noVowels[0] + noVowels[1];
	} else {
		my_string = name[0] + name[1];
	}

	return my_string.toUpperCase();
}

export function indexOfMax(arr) {
	if (arr.length === 0) {
		return -1;
	}

	var max = arr[0];
	var maxIndex = 0;

	for (var i = 1; i < arr.length; i++) {
		if (arr[i] > max) {
			maxIndex = i;
			max = arr[i];
		}
	}

	return maxIndex;
}

export function indexOfMin(arr) {
	if (arr.length === 0) {
		return -1;
	}

	var min = arr[0];
	var minIndex = 0;

	for (var i = 1; i < arr.length; i++) {
		if (arr[i] < min) {
			minIndex = i;
			min = arr[i];
		}
	}

	return minIndex;
}

///GPS///
export function distance(lat1, lon1, lat2, lon2) {
	var p = 0.017453292519943295;
	var c = Math.cos;
	var a = 0.5 - c((lat2 - lat1) * p) / 2 + (c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p))) / 2;

	return (12742 * Math.asin(Math.sqrt(a)) * 1000).toFixed(0);
}

// Converts from degrees to radians.
export function toRadians(degrees) {
	return (degrees * Math.PI) / 180;
}

// Converts from radians to degrees.
export function toDegrees(radians) {
	return (radians * 180) / Math.PI;
}

export function bearing(startLat, startLng, destLat, destLng) {
	startLat = this.toRadians(startLat);
	startLng = this.toRadians(startLng);
	destLat = this.toRadians(destLat);
	destLng = this.toRadians(destLng);

	const y = Math.sin(destLng - startLng) * Math.cos(destLat);
	const x =
		Math.cos(startLat) * Math.sin(destLat) - Math.sin(startLat) * Math.cos(destLat) * Math.cos(destLng - startLng);
	let brng = Math.atan2(y, x);
	brng = this.toDegrees(brng);
	return (brng + 360) % 360;
}
