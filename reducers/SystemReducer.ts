import { SET_DEVMODE, SET_FIGHTS } from "../actions/SystemActions";
import { FightState } from "./PlayerReducer";

export interface SystemState {
  devMode: boolean;
  fights: FightState[];
}

export function SystemReducer(
  state: SystemState = { devMode: false, fights: [] },
  action
) {
  switch (action.type) {
    case SET_DEVMODE:
      return { ...state, devMode: action.payload };
    case SET_FIGHTS:
      return { ...state, fights: action.payload };
    default:
      return state;
  }
}
