import { SET_BOMBS } from "../actions/PlayerActions";

export interface Bomb {
  lat: number;
  long: number;
  team: number;
  id: string;
  name: string;
}

export function BombsReducer(state: Bomb[] = null, action) {
  switch (action.type) {
    case SET_BOMBS:
      return action.payload;
    default:
      return state;
  }
}
