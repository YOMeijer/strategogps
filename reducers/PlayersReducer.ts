import { PlayerDTO } from "./PlayerReducer";
import { SET_PLAYERS } from "../actions/PlayerActions";

export function PlayersReducer(state: PlayerDTO[] = null, action) {
  switch (action.type) {
    case SET_PLAYERS:
      return action.payload;
    default:
      return state;
  }
}
