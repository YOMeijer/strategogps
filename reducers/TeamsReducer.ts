import { FETCH_TEAMS } from "../actions/Actions";

export default function(state = null, action) {
  switch (action.type) {
    case FETCH_TEAMS:
      return action.payload;
    default:
      return state;
  }
}
