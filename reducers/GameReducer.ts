import { SET_GAME_INFO } from "../actions/SystemActions";

export interface GameState {
	score: {
		team1: number;
		team2: number;
	};
	bombs: {
		team1: number;
		team2: number;
	};
	questions: {
		team1: number;
		team2: number;
	};
	hqContested: boolean;
	startingGame: boolean;
	playerLimit: number[];
	gameEnd: boolean;
}

export function GameReducer(
	state: GameState = {
		score: null,
		hqContested: false,
		startingGame: false,
		questions: null,
		bombs: null,
		playerLimit: null,
		gameEnd: false
	},
	action
) {
	switch (action.type) {
		case SET_GAME_INFO:
			return action.payload;
		default:
			return state;
	}
}
