import { SET_QUESTIONS } from "../actions/PlayerActions";

export interface Question {
	lat: number;
	long: number;
	id: string;
	team: number;
}

export function QuestionsReducer(state: Question[] = null, action) {
	switch (action.type) {
		case SET_QUESTIONS:
			return action.payload;
		default:
			return state;
	}
}
