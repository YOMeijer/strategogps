import { SET_HQS } from "../actions/PlayerActions";

export interface HQ {
	lat: number;
	long: number;
	id: string;
	active: boolean;
	startTime?: number;
}

export function HQsReducer(state: HQ[] = null, action) {
	switch (action.type) {
		case SET_HQS:
			return action.payload;
		default:
			return state;
	}
}
