import { SET_USER_AUTH, SET_PLAYER_INFO } from "../actions/UserActions";
import { User } from "firebase";
import { SET_FIGHT_INFO } from "../actions/PlayerActions";

export function AuthReducer(state: User = null, action) {
	switch (action.type) {
		case SET_USER_AUTH:
			return action.payload;
		default:
			return state;
	}
}

export interface PlayerDTO {
	uid: string;
	lastHQTime: number;
	playerInfo: PlayerState;
	hqId: string;
}

export interface PlayerState {
	name: string;
	type: number;
	team: number;
	rank: number;
	isDead: boolean;
	killedBy: string;
	killedByRank: number;
	timeOfDeath: number;
	lastAbilityUsed?: number;
	reviveCode?: number;
}

export function PlayerReducer(state: PlayerState = null, action) {
	switch (action.type) {
		case SET_PLAYER_INFO:
			return action.payload;
		default:
			return state;
	}
}

export interface FightState {
	uid?: string;
	id: number;
	attackTeam: number;
	attackRank: number;
	attackName: string;
	defendTeam?: number;
	defendRank?: number;
	defendName?: string;
	defendUid?: string;
}

export function PlayerFightReducer(state: FightState = null, action) {
	switch (action.type) {
		case SET_FIGHT_INFO:
			console.log(action.payload);
			return action.payload;
		default:
			return state;
	}
}
