import { SET_COORDS } from "../actions/GPSActions";

export function CoordsReducer(state: Coordinates = null, action) {
	switch (action.type) {
		case SET_COORDS:
			return action.payload;
		default:
			return state;
	}
}
