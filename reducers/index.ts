import { combineReducers } from "redux";
import {
  AuthReducer,
  PlayerReducer,
  PlayerFightReducer,
  PlayerState,
  FightState,
  PlayerDTO
} from "./PlayerReducer";
import { SystemReducer, SystemState } from "./SystemReducer";
import { BombsReducer, Bomb } from "./BombsReducer";
import { CoordsReducer } from "./CoordsReducer";
import { HQsReducer, HQ } from "./HQsReducer";
import { User } from "firebase";
import { Question, QuestionsReducer } from "./QuestionsReducer";
import { GameState, GameReducer } from "./GameReducer";
import { PlayersReducer } from "./PlayersReducer";

export interface AppStore {
  auth: User;
  player: PlayerState;
  coords: Coordinates;
  bombs: Bomb[];
  hqs: HQ[];
  questions: Question[];
  fight: FightState;
  system: SystemState;
  game: GameState;
  players: PlayerDTO[];
}

const rootReducer = combineReducers({
  auth: AuthReducer,
  player: PlayerReducer,
  coords: CoordsReducer,
  bombs: BombsReducer,
  hqs: HQsReducer,
  questions: QuestionsReducer,
  fight: PlayerFightReducer,
  system: SystemReducer,
  game: GameReducer,
  players: PlayersReducer
});

export default rootReducer;
