import * as React from "react";
import _ from "lodash";

type ThisState = {
	boo: string;
};

type ThisProps = {
	onMarkQuestion: Function;
};

export default class MarkQuestion extends React.Component<ThisProps, ThisState> {
	componentWillMount() {}

	componentWillReceiveProps(nextProps) {}

	markQuestion(team: number) {
		this.props.onMarkQuestion(team);
	}

	render() {
		return (
			<div>
				<button onClick={() => this.markQuestion(0)}>Opdracht plaatsen</button>
				<button onClick={() => this.markQuestion(1)}>Opdracht plaatsen(Blauw)</button>
				<button onClick={() => this.markQuestion(2)}>Opdracht plaatsen(Rood)</button>
			</div>
		);
	}
}
