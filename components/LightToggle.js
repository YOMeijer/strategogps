import React, { Component } from "react";
import _ from "lodash";
import { controller, yeelight } from "../Firebase.ts";

export default class LightToggle extends Component {
  constructor() {
    super();

    this.state = {};
  }

  componentWillMount() {}

  componentWillReceiveProps(nextProps) {}

  lightSwitch(cmnd) {
    controller
      .child("commands")
      .push({ path: `yeelight/${this.props.name}/on`, command: cmnd });
    yeelight.child(this.props.name).update({ loading: true });
  }

  renderLightButton() {
    return (
      <div className="light-toggle">
        <div>{this.props.name}</div>
        {this.props.light.loading ? (
          <div className="light-btn light-wait">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              version="1.1"
              viewBox="0 0 15 15"
            >
              <g>
                <path d="M10.5,1.674V4c1.215,0.912,2,2.364,2,4c0,2.762-2.238,5-5,5s-5-2.238-5-5c0-1.636,0.785-3.088,2-4   V1.674C2.135,2.797,0.5,5.208,0.5,8c0,3.866,3.134,7,7,7s7-3.134,7-7C14.5,5.208,12.865,2.797,10.5,1.674z" />
                <path d="M8.5,7.003V0.997C8.5,0.446,8.056,0,7.5,0c-0.553,0-1,0.453-1,0.997v6.006C6.5,7.554,6.944,8,7.5,8   C8.053,8,8.5,7.547,8.5,7.003z" />
              </g>
            </svg>
          </div>
        ) : this.props.light.on ? (
          <div
            onClick={() => this.lightSwitch(false)}
            className="light-btn light-on"
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              version="1.1"
              viewBox="0 0 15 15"
            >
              <g>
                <path d="M10.5,1.674V4c1.215,0.912,2,2.364,2,4c0,2.762-2.238,5-5,5s-5-2.238-5-5c0-1.636,0.785-3.088,2-4   V1.674C2.135,2.797,0.5,5.208,0.5,8c0,3.866,3.134,7,7,7s7-3.134,7-7C14.5,5.208,12.865,2.797,10.5,1.674z" />
                <path d="M8.5,7.003V0.997C8.5,0.446,8.056,0,7.5,0c-0.553,0-1,0.453-1,0.997v6.006C6.5,7.554,6.944,8,7.5,8   C8.053,8,8.5,7.547,8.5,7.003z" />
              </g>
            </svg>
          </div>
        ) : !this.props.light.on ? (
          <div
            onClick={() => this.lightSwitch(true)}
            className="light-btn light-off"
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              version="1.1"
              viewBox="0 0 15 15"
            >
              <g>
                <path d="M10.5,1.674V4c1.215,0.912,2,2.364,2,4c0,2.762-2.238,5-5,5s-5-2.238-5-5c0-1.636,0.785-3.088,2-4   V1.674C2.135,2.797,0.5,5.208,0.5,8c0,3.866,3.134,7,7,7s7-3.134,7-7C14.5,5.208,12.865,2.797,10.5,1.674z" />
                <path d="M8.5,7.003V0.997C8.5,0.446,8.056,0,7.5,0c-0.553,0-1,0.453-1,0.997v6.006C6.5,7.554,6.944,8,7.5,8   C8.053,8,8.5,7.547,8.5,7.003z" />
              </g>
            </svg>
          </div>
        ) : null}
      </div>
    );
  }

  render() {
    return <div>{this.renderLightButton()}</div>;
  }
}
