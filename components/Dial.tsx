import * as React from "react";
import _ from "lodash";
import RankIcon from "./RankIcon";

type ThisState = {
	dialWidth: number;
	prevBearing: number;
	bearingDif: number;
};

type ThisProps = {
	distance: number;
	bearing: number;
	relativeBearing: number;
	accuracy: number;
	speed: number;
	rank: number;
	name: string;
	contested?: boolean;
	team?: number;
};

export default class Dial extends React.Component<ThisProps, ThisState> {
	componentWillMount() {
		this.setState({ dialWidth: 0, prevBearing: 0 });
	}

	componentWillReceiveProps(nextProps: ThisProps) {
		console.log(nextProps.contested);
		if (nextProps.distance) {
			const d = nextProps.distance;
			if (d > 100) {
				this.setState({ dialWidth: 30 });
			} else if (d <= 10) {
				this.setState({ dialWidth: 260 });
			} else {
				this.setState({
					dialWidth: 170 - ((((d - 10) / 90) * 100) / 100) * 140
				});
			}
		}
		if (nextProps.relativeBearing) {
			this.setState({
				bearingDif: Math.abs(this.state.prevBearing - nextProps.relativeBearing),
				prevBearing: nextProps.relativeBearing
			});
		}
	}

	degToCompass(num) {
		var val = Math.floor(num / 22.5 + 0.5);
		var arr = ["N", "NNO", "NO", "ONO", "O", "OZO", "ZO", "ZO", "Z", "ZW", "ZW", "WZW", "W", "WNW", "NW", "NNW"];
		return arr[val % 16];
	}

	render() {
		return (
			<div className="gps-dial-container">
				<div
					style={{
						width: "100vmin",
						height: "100vmin",
						position: "absolute",
						margin: "0vmin",
						transform: "rotate(" + (-90 + this.props.relativeBearing) + "deg)",
						borderRadius: "80vmin",
						transition: "all 5s ease"
					}}>
					<svg height="100%" width="100%" viewBox="0 0 100 100" className="gps-dial">
						<g className="gps-outer-dial">
							<circle
								cx="50"
								cy="50"
								r="40"
								stroke={this.props.contested ? (this.props.team === 2 ? "rgb(0, 71, 177)" : "rgb(177, 0, 0)") : "white"}
								strokeWidth="3"
								strokeDasharray={this.state.dialWidth + " 900"}
								fill="transparent"
								strokeLinecap="round"
								transform={"rotate(-" + (this.state.dialWidth / 360) * 260 + ", 50, 50)"}
							/>
						</g>
						<g className="gps-inner-dial">
							<circle
								cx="50"
								cy="50"
								r="40"
								stroke={this.props.contested ? (this.props.team === 2 ? "rgb(0, 71, 177)" : "rgb(177, 0, 0)") : "white"}
								strokeWidth="3"
								strokeDasharray={this.state.dialWidth + " 900"}
								fill="transparent"
								strokeLinecap="round"
								transform={"rotate(-" + (this.state.dialWidth / 360) * 260 + ", 50, 50)"}
							/>
						</g>
						<g className="gps-inner-inner-dial">
							<circle
								cx="50"
								cy="50"
								r="40"
								stroke={this.props.contested ? (this.props.team === 2 ? "rgb(0, 71, 177)" : "rgb(177, 0, 0)") : "white"}
								strokeWidth="3"
								strokeDasharray={this.state.dialWidth + " 900"}
								fill="transparent"
								strokeLinecap="round"
								transform={"rotate(-" + (this.state.dialWidth / 360) * 260 + ", 50, 50)"}
							/>
						</g>
					</svg>
				</div>
				<div className="gps-dial-text">
					<p>{this.props.name}</p>
					<RankIcon size="40vmin" rank={this.props.rank} />
					<strong style={{ marginTop: "-2vmin" }}>
						{this.props.distance}m, {this.degToCompass(this.props.bearing)}
					</strong>
					<strong style={{ fontSize: "4vmin", marginTop: "-2vmin" }}>(+/- {this.props.accuracy}m)</strong>
				</div>
			</div>
		);
	}
}
