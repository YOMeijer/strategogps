import React, { Component } from "react";
import _ from "lodash";
import { connect } from "react-redux";
import { getTeams } from "../actions/Actions";

class Devices extends Component {
  constructor() {
    super();

    this.state = {};
  }

  componentWillMount() {
    this.props.getTeams();
  }

  componentWillReceiveProps(nextProps) {}

  renderDevices() {
    return _.map(this.props.teams, (device, key) => {
      if (key !== "unknown") {
        return (
          <div key={key} className="device">
            <div>
              <div>{device.name}</div>
              <div>{device.ip}</div>
            </div>

            <div className={"online-dot " + device.online} />
          </div>
        );
      } else {
        return <div>Unknown</div>;
      }
    });
  }

  render() {
    return <div className="devices">{this.renderDevices()}</div>;
  }
}

function mapStateToProps(state) {
  return {
    teams: state.teams
  };
}

export default connect(
  mapStateToProps,
  { getTeams }
)(Devices);
