import * as React from "react";
import _ from "lodash";
import RankIcon from "./RankIcon";

type ThisState = {};

type ThisProps = {
	accuracy: number;
	rank: number;
	message?: string;
	name: string;
	abilityDisabled?: boolean;
	onRankSwitch?();
	onRevive?();
};

export default class InactiveDial extends React.Component<ThisProps, ThisState> {
	componentWillMount() {}

	componentWillReceiveProps(nextProps: ThisProps) {}

	render() {
		return (
			<div className="gps-dial-container inactive">
				<div
					style={{
						width: "100vmin",
						height: "100vmin",
						position: "absolute",
						margin: "0vmin",
						borderRadius: "80vmin",
						transition: "all 5s ease"
					}}>
					<svg height="100%" width="100%" viewBox="0 0 100 100" className="gps-dial" />
				</div>
				<div className="gps-dial-text">
					<p>{this.props.name}</p>
					<RankIcon size="40vmin" rank={this.props.rank} />
					<p style={{ marginTop: "3vmin" }}>{this.props.message}</p>
					<p>(GPS: +/- {this.props.accuracy}m)</p>
					{this.props.rank === 10 ? (
						<button className="btn-reg" onClick={() => this.props.onRankSwitch()} disabled={this.props.abilityDisabled}>
							Team wijzigen
						</button>
					) : null}
					{this.props.rank === 4 ? (
						<button className="btn-reg" onClick={() => this.props.onRevive()} disabled={this.props.abilityDisabled}>
							Helen
						</button>
					) : null}
				</div>
			</div>
		);
	}
}
