import * as React from "react";
import * as _ from "lodash";
import { connect } from "react-redux";
import { bombs, game } from "../../Firebase";
import { PlayerState } from "../../reducers/PlayerReducer";
import { Bomb } from "../../reducers/BombsReducer";
import { distance, indexOfMin, randomString, bearing } from "../../shared/shared.functions";
import Dial from "../Dial";
import InactiveDial from "../InactiveDial";
import { AppStore } from "../../reducers";
import { GameState } from "../../reducers/GameReducer";
import RankIcon from "../RankIcon";

type ThisState = {
	bombTarget: Bomb;
	distance: number;
	bearing: number;
	relativeBearing: number;
	myBombs: number;
	bombsAvailable: number;
};

type ThisProps = {
	player: PlayerState;
	coords: Coordinates;
	bombs: Bomb[];
	isNearCircle: boolean;
	game: GameState;
};

class Mineur extends React.Component<ThisProps, ThisState> {
	componentWillMount() {
		this.setState({ bombTarget: null, distance: 999, bombsAvailable: 0 });
	}

	componentWillReceiveProps(nextProps: ThisProps) {
		if (nextProps.coords) {
			const filteredBombs = _.filter(nextProps.bombs, bomb => {
				if (this.props.player.team !== bomb.team) {
					return true;
				}
			});
			const distanceArray = _.map(filteredBombs, bomb => {
				return parseInt(distance(bomb.lat, bomb.long, nextProps.coords.latitude, nextProps.coords.longitude));
			});

			if (distanceArray.length > 0) {
				const target = filteredBombs[indexOfMin(distanceArray)];
				this.setState({
					bombTarget: target,
					distance: parseInt(distance(nextProps.coords.latitude, nextProps.coords.longitude, target.lat, target.long)),
					relativeBearing:
						parseInt(
							bearing(nextProps.coords.latitude, nextProps.coords.longitude, target.lat, target.long).toFixed()
						) - nextProps.coords.heading,
					bearing: parseInt(
						bearing(nextProps.coords.latitude, nextProps.coords.longitude, target.lat, target.long).toFixed()
					)
				});
			} else {
				this.setState({ bombTarget: null });
			}

			const myBombs = _.filter(nextProps.bombs, bomb => {
				if (this.props.player.team === bomb.team) {
					return true;
				}
			}).length;
			this.setState({ myBombs: myBombs });
		}

		if (nextProps.game) {
			this.setState({ bombsAvailable: nextProps.game.bombs["team" + nextProps.player.team] });
		}
	}

	plantBomb() {
		const bombId = randomString(10);
		const newBomb: Bomb = {
			lat: this.props.coords.latitude,
			long: this.props.coords.longitude,
			team: this.props.player.team,
			id: bombId,
			name: this.props.player.name
		};
		bombs.child(bombId).set(newBomb);
		game
			.child("bombs")
			.child("team" + this.props.player.team)
			.set(this.props.game.bombs["team" + this.props.player.team] - 1);
	}

	disarmBomb() {
		bombs
			.child(this.state.bombTarget.id)
			.remove()
			.then(res => {
				this.setState({ bombTarget: null, distance: 999 });
			});
	}

	renderBombsAvailable() {
		return;
	}

	render() {
		return (
			<div>
				{this.state.distance < 200 && this.state.bombTarget ? (
					<Dial
						name={this.props.player.name}
						distance={this.state.distance}
						relativeBearing={this.state.relativeBearing}
						bearing={this.state.bearing}
						accuracy={this.props.coords.accuracy}
						speed={this.props.coords.speed}
						rank={this.props.player.rank}
					/>
				) : (
					<InactiveDial
						name={this.props.player.name}
						accuracy={this.props.coords ? this.props.coords.accuracy : 999}
						rank={this.props.player.rank}
						message={"no bombs in range..."}
					/>
				)}
				<div className="flx-row">
					{this.state.bombsAvailable >= 1 ? <RankIcon rank={0} size={"15vmin"} /> : null}
					{this.state.bombsAvailable >= 2 ? <RankIcon rank={0} size={"15vmin"} /> : null}
				</div>
				<br />
				<div className="flx-col">
					<button
						className="btn-reg"
						onClick={() => this.plantBomb()}
						disabled={
							this.props.isNearCircle ||
							this.state.myBombs >= 3 ||
							this.state.bombsAvailable === 0 ||
							this.props.coords.accuracy > 17
						}>
						Bom plaatsen
					</button>
					<br />
					{this.state.distance < 7 ? (
						<button className="btn-reg" onClick={() => this.disarmBomb()}>
							Bom ontmantelen
						</button>
					) : null}
				</div>
				<div />
			</div>
		);
	}
}

function mapStateToProps(state: AppStore) {
	return { player: state.player, coords: state.coords, bombs: state.bombs, game: state.game };
}

export default connect(
	mapStateToProps,
	{}
)(Mineur);
