import * as React from "react";
import * as _ from "lodash";
import { connect } from "react-redux";
import { players } from "../../Firebase";
import { PlayerState } from "../../reducers/PlayerReducer";
import { distance, bearing } from "../../shared/shared.functions";
import Dial from "../Dial";
import InactiveDial from "../InactiveDial";

type ThisState = {
	rankInterval: any;
	playerToTrack: { accuracy: number; lat: number; long: number };
	distance: number;
	bearing: number;
	relativeBearing: number;
};

type ThisProps = {
	player: PlayerState;
	coords: Coordinates;
};

class Spion extends React.Component<ThisProps, ThisState> {
	componentWillMount() {
		this.setState({
			rankInterval: setInterval(() => {
				this.findRank10();
			}, 10000)
		});
	}

	componentDidMount() {
		this.findRank10();
	}

	componentWillUnmount() {
		clearInterval(this.state.rankInterval);
		this.setState({ rankInterval: null });
	}

	componentWillReceiveProps(nextProps) {}

	findRank10() {
		const rank10 = players.orderByChild("playerInfo/rank").equalTo(10);
		const myTeam = this.props.player.team;
		rank10.once("value", snapshot => {
			const playerToTrack = _.filter(snapshot.val(), function(player) {
				return player.playerInfo.team !== myTeam;
			})[0];

			if (playerToTrack && this.props.coords) {
				const target = playerToTrack.location;
				this.setState({
					playerToTrack: target,
					distance: parseInt(
						distance(this.props.coords.latitude, this.props.coords.longitude, target.lat, target.long)
					),
					relativeBearing:
						parseInt(
							bearing(this.props.coords.latitude, this.props.coords.longitude, target.lat, target.long).toFixed()
						) - this.props.coords.heading,
					bearing: parseInt(
						bearing(this.props.coords.latitude, this.props.coords.longitude, target.lat, target.long).toFixed()
					)
				});
			} else {
				this.setState({ playerToTrack: null });
			}
		});
	}

	render() {
		return (
			<div>
				<div>
					{this.state.playerToTrack ? (
						<Dial
							name={this.props.player.name}
							distance={this.state.distance}
							relativeBearing={this.state.relativeBearing}
							bearing={this.state.bearing}
							accuracy={this.props.coords.accuracy}
							speed={this.props.coords.speed}
							rank={this.props.player.rank}
						/>
					) : (
						<InactiveDial
							name={this.props.player.name}
							accuracy={this.props.coords ? this.props.coords.accuracy : 999}
							rank={this.props.player.rank}
							message={"Geen maarschalk gevonden..."}
						/>
					)}
				</div>
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		player: state.player,
		coords: state.coords
	};
}

export default connect(
	mapStateToProps,
	{}
)(Spion);
