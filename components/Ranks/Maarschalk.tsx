import * as React from "react";
import * as _ from "lodash";
import { connect } from "react-redux";
import { bombs, players } from "../../Firebase";
import { PlayerState, PlayerDTO } from "../../reducers/PlayerReducer";
import InactiveDial from "../InactiveDial";
import { playersOnce } from "../../actions/PlayerActions";
import { AppStore } from "../../reducers";
import RankIcon from "../RankIcon";
import { User } from "firebase";
import { msToTime } from "../../shared/shared.functions";
import { GameState } from "../../reducers/GameReducer";

type ThisState = {
	showPlayers: boolean;
	showRanks: boolean;
	selectedPlayer: string;
	selectedPlayerRank: number;
	lastAbilityUsed: string;
	abilityTimer: any;
	playerAmount: number[];
};

type ThisProps = {
	player: PlayerState;
	coords: Coordinates;
	players: PlayerDTO[];
	auth: User;
	playersOnce(team: number);
	isNearCircle: boolean;
	game: GameState;
};

class Maarschalk extends React.Component<ThisProps, ThisState> {
	componentWillMount() {
		this.setState({
			showRanks: false,
			showPlayers: false,
			abilityTimer: setInterval(() => {
				this.checkTimeAbility();
			}, 1000)
		});
	}

	componentWillReceiveProps(nextProps: ThisProps) {
		if (nextProps.player) {
			if (nextProps.player.lastAbilityUsed) {
			}
		}
	}

	checkTimeAbility() {
		const now = new Date().getTime();

		if (now - this.props.player.lastAbilityUsed > 1000 * 60) {
			this.setState({
				lastAbilityUsed: null
			});
		} else {
			this.setState({
				lastAbilityUsed: msToTime(1000 * 60 - (now - this.props.player.lastAbilityUsed))
			});
		}
	}

	componentWillUnmount() {
		clearInterval(this.state.abilityTimer);
		this.setState({ abilityTimer: null });
	}

	openRankSwitch(b: boolean) {
		this.setState({ showPlayers: b, showRanks: false });
		if (b) {
			this.props.playersOnce(this.props.player.team);
		}
	}

	selectPlayer(uid: string, rank: number) {
		this.setState({ showRanks: true, selectedPlayer: uid, selectedPlayerRank: rank });
		this.getPlayerAmount();
	}

	getPlayerAmount() {
		const p = _.filter(
			_.orderBy(
				_.map(
					_.filter(this.props.players, player => {
						return player.playerInfo.type === 1 ? true : false;
					}),
					player => {
						return player;
					}
				),
				function(pl: PlayerDTO) {
					return pl.playerInfo.rank;
				}
			),
			player => {
				return player.playerInfo.rank === 10 ? false : true;
			}
		);

		let count = _.map(new Array(11), function(el, i) {
			return 0;
		});

		p.forEach(player => {
			count[player.playerInfo.rank]++;
		});
		this.setState({ playerAmount: count });
	}

	changeRank(rank: number) {
		players
			.child(this.state.selectedPlayer)
			.child("playerInfo")
			.update({ rank: rank })
			.then(res => {
				this.props.playersOnce(this.props.player.team);
				this.setState({ showRanks: false });
			});
		if (!this.props.game.startingGame) {
			players
				.child(this.props.auth.uid)
				.child("playerInfo")
				.update({ lastAbilityUsed: new Date().getTime() });
		}
	}

	renderPlayers() {
		if (this.props.players) {
			return _.map(this.props.players, player => {
				if (player.playerInfo.rank !== 10) {
					return (
						<div
							key={player.playerInfo.name}
							className="flx-row-wide maarschalk-player"
							onClick={() => this.selectPlayer(player.uid, player.playerInfo.rank)}>
							<strong className="lg-text" style={{ padding: "3vmin" }}>
								{player.playerInfo.name}
							</strong>
							<div>
								<RankIcon rank={player.playerInfo.rank} size={"20vmin"} />
							</div>
						</div>
					);
				} else {
					return null;
				}
			});
		} else {
			return null;
		}
	}

	checkRankAvailable(r: number) {
		if (this.state.selectedPlayerRank !== r && this.props.game.playerLimit[r] > this.state.playerAmount[r]) {
			return true;
		} else {
			return false;
		}
	}

	render() {
		return (
			<div>
				{this.state.showRanks ? (
					<div>
						<button className="btn-close" onClick={() => this.openRankSwitch(false)}>
							X
						</button>
						<br />
						<br />
						<div className="flx-row ranks">
							{this.checkRankAvailable(1) ? (
								<div style={{ margin: "5vmin" }} onClick={() => this.changeRank(1)}>
									<RankIcon rank={1} size={"20vmin"} />
								</div>
							) : null}
							{this.checkRankAvailable(2) ? (
								<div style={{ margin: "5vmin" }} onClick={() => this.changeRank(2)}>
									<RankIcon rank={2} size={"20vmin"} />
								</div>
							) : null}
							{this.checkRankAvailable(3) ? (
								<div style={{ margin: "5vmin" }} onClick={() => this.changeRank(3)}>
									<RankIcon rank={3} size={"20vmin"} />
								</div>
							) : null}
							{this.checkRankAvailable(4) ? (
								<div style={{ margin: "5vmin" }} onClick={() => this.changeRank(4)}>
									<RankIcon rank={4} size={"20vmin"} />
								</div>
							) : null}
							{this.checkRankAvailable(5) ? (
								<div style={{ margin: "5vmin" }} onClick={() => this.changeRank(5)}>
									<RankIcon rank={5} size={"20vmin"} />
								</div>
							) : null}
							{this.checkRankAvailable(6) ? (
								<div style={{ margin: "5vmin" }} onClick={() => this.changeRank(6)}>
									<RankIcon rank={6} size={"20vmin"} />
								</div>
							) : null}
							{this.checkRankAvailable(7) ? (
								<div style={{ margin: "5vmin" }} onClick={() => this.changeRank(7)}>
									<RankIcon rank={7} size={"20vmin"} />
								</div>
							) : null}
							{this.checkRankAvailable(8) ? (
								<div style={{ margin: "5vmin" }} onClick={() => this.changeRank(8)}>
									<RankIcon rank={8} size={"20vmin"} />
								</div>
							) : null}
							{this.checkRankAvailable(9) ? (
								<div style={{ margin: "5vmin" }} onClick={() => this.changeRank(9)}>
									<RankIcon rank={9} size={"20vmin"} />
								</div>
							) : null}
						</div>
					</div>
				) : this.state.showPlayers ? (
					<div>
						<button className="btn-close" onClick={() => this.openRankSwitch(false)}>
							X
						</button>
						<br />
						<br />
						{this.state.lastAbilityUsed ? <div className="flx-row lg-text">{this.state.lastAbilityUsed}</div> : null}
						<div
							style={{
								opacity: this.state.lastAbilityUsed ? 0.5 : 1,
								pointerEvents: this.state.lastAbilityUsed ? "none" : "all"
							}}>
							{this.renderPlayers()}
						</div>
					</div>
				) : (
					<InactiveDial
						name={this.props.player.name}
						rank={this.props.player.rank}
						accuracy={this.props.coords ? this.props.coords.accuracy : 999}
						onRankSwitch={() => this.openRankSwitch(true)}
						abilityDisabled={this.props.isNearCircle}
					/>
				)}
			</div>
		);
	}
}

function mapStateToProps(state: AppStore) {
	return { player: state.player, coords: state.coords, players: state.players, auth: state.auth, game: state.game };
}

export default connect(
	mapStateToProps,
	{ playersOnce }
)(Maarschalk);
