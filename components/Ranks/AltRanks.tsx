import * as React from "react";
import * as _ from "lodash";
import { connect } from "react-redux";
import { bombs } from "../../Firebase";
import { PlayerState } from "../../reducers/PlayerReducer";
import { Bomb } from "../../reducers/BombsReducer";
import InactiveDial from "../InactiveDial";
import { GameState } from "../../reducers/GameReducer";
import Score from "../Score";

type ThisState = {};

type ThisProps = {
	player: PlayerState;
	coords: Coordinates;
	bombs: Bomb[];
	game: GameState;
};

class Sergeant extends React.Component<ThisProps, ThisState> {
	componentWillMount() {
		this.setState({});
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.coords) {
		}
	}

	render() {
		return (
			<div>
				{this.props.game && this.props.game.score ? <Score /> : null}
				<InactiveDial
					name={this.props.player.name}
					rank={this.props.player.rank}
					accuracy={this.props.coords ? this.props.coords.accuracy : 999}
				/>
			</div>
		);
	}
}

function mapStateToProps(state) {
	return { player: state.player, coords: state.coords, bombs: state.bombs, game: state.game };
}

export default connect(
	mapStateToProps,
	{}
)(Sergeant);
