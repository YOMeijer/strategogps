import * as React from "react";
import * as _ from "lodash";
import { connect } from "react-redux";
import { PlayerState } from "../../reducers/PlayerReducer";
import { Bomb } from "../../reducers/BombsReducer";
import { distance, indexOfMin, bearing } from "../../shared/shared.functions";
import Dial from "../Dial";
import { Question } from "../../reducers/QuestionsReducer";
import { questionsListener } from "../../actions/PlayerActions";
import InactiveDial from "../InactiveDial";
import { questions, game } from "../../Firebase";
import { GameState } from "../../reducers/GameReducer";

type ThisState = {
	questionTarget: Question;
	distance: number;
	bearing: number;
	relativeBearing: number;
};

type ThisProps = {
	player: PlayerState;
	coords: Coordinates;
	questions: Question[];
	questionsListener();
	game: GameState;
};

class Luitenant extends React.Component<ThisProps, ThisState> {
	componentWillMount() {
		this.setState({});
		this.props.questionsListener();
	}

	componentWillReceiveProps(nextProps: ThisProps) {
		console.log(nextProps);
		if (nextProps.coords) {
			const distanceArray = _.map(
				_.filter(nextProps.questions, q => {
					const enemy = this.props.player.team === 1 ? 2 : 1;
					return q.team !== enemy ? true : false;
				}),
				question => {
					return parseInt(distance(question.lat, question.long, nextProps.coords.latitude, nextProps.coords.longitude));
				}
			);
			if (distanceArray.length > 0) {
				const target = nextProps.questions[indexOfMin(distanceArray)];
				this.setState({
					questionTarget: target,
					distance: parseInt(distance(nextProps.coords.latitude, nextProps.coords.longitude, target.lat, target.long)),
					relativeBearing:
						parseInt(
							bearing(nextProps.coords.latitude, nextProps.coords.longitude, target.lat, target.long).toFixed()
						) - nextProps.coords.heading,
					bearing: parseInt(
						bearing(nextProps.coords.latitude, nextProps.coords.longitude, target.lat, target.long).toFixed()
					)
				});
			} else {
				this.setState({ questionTarget: null });
			}

			if (this.state.questionTarget && this.state.distance < 11) {
				game
					.child("questions")
					.child("team" + this.props.player.team)
					.set(this.props.game.questions["team" + this.props.player.team] + 1);
				questions.child(this.state.questionTarget.id).remove();
			}
		}
	}

	render() {
		return (
			<div>
				{this.state.distance < 250 && this.state.questionTarget ? (
					<Dial
						name={this.props.player.name}
						distance={this.state.distance}
						relativeBearing={this.state.relativeBearing}
						bearing={this.state.bearing}
						accuracy={this.props.coords.accuracy}
						speed={this.props.coords.speed}
						rank={this.props.player.rank}
					/>
				) : (
					<InactiveDial
						name={this.props.player.name}
						accuracy={this.props.coords ? this.props.coords.accuracy : 999}
						rank={this.props.player.rank}
						message={"no questions in range..."}
					/>
				)}
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		player: state.player,
		coords: state.coords,
		questions: state.questions,
		game: state.game
	};
}

export default connect(
	mapStateToProps,
	{ questionsListener }
)(Luitenant);
