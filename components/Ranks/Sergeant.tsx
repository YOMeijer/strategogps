import * as React from "react";
import * as _ from "lodash";
import { connect } from "react-redux";
import { PlayerState } from "../../reducers/PlayerReducer";
import InactiveDial from "../InactiveDial";
import { players } from "../../Firebase";
import { distance } from "../../shared/shared.functions";

type ThisState = {
	revive: boolean;
	reviveCodeInvalid: boolean;
	reviveError: string;
	reviveInput: string;
};

type ThisProps = {
	player: PlayerState;
	coords: Coordinates;
	isNearCircle: boolean;
};

class Sergeant extends React.Component<ThisProps, ThisState> {
	componentWillMount() {
		this.setState({});
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.coords) {
		}
	}

	openRevive(b: boolean) {
		this.setState({ revive: b });
	}

	handleReviveInput(n: string) {
		this.setState({ reviveInput: n, reviveCodeInvalid: false });
	}

	reviveNow() {
		console.log(this.state.reviveInput);
		const playerRef = players.orderByChild("playerInfo/reviveCode").equalTo(parseInt(this.state.reviveInput));
		playerRef.once("value", snapshot => {
			if (!snapshot.val()) {
				this.setState({ reviveCodeInvalid: true, reviveError: "Code is ongeldig..." });
			} else {
				const pr = _.map(snapshot.val(), (player, uid) => {
					return { ...player, uid: uid };
				})[0];

				if (
					pr.playerInfo.team !== this.props.player.team ||
					!pr.playerInfo.isDead ||
					parseInt(
						distance(pr.location.lat, pr.location.long, this.props.coords.latitude, this.props.coords.longitude)
					) > 20
				) {
					this.setState({ reviveCodeInvalid: true, reviveError: "Speler te ver weg..." });
				} else {
					players
						.child(pr.uid)
						.child("playerInfo")
						.update({ isDead: false })
						.then(res => {
							this.setState({ revive: false, reviveCodeInvalid: false });
						});
				}
			}
		});
	}

	render() {
		return (
			<div>
				{this.state.revive ? (
					<div>
						<button className="btn-close" onClick={() => this.openRevive(false)}>
							X
						</button>
						<div className="flx-col">
							{this.state.reviveCodeInvalid ? <div>{this.state.reviveError}</div> : null}
							<br />
							<input autoFocus type="number" onChange={event => this.handleReviveInput(event.target.value)} />
							<br />
							<button
								className="btn-reg"
								onClick={() => {
									this.reviveNow();
								}}>
								Helen
							</button>
						</div>
					</div>
				) : (
					<InactiveDial
						name={this.props.player.name}
						rank={this.props.player.rank}
						accuracy={this.props.coords ? this.props.coords.accuracy : 999}
						onRevive={() => this.openRevive(true)}
						abilityDisabled={this.props.isNearCircle}
					/>
				)}
			</div>
		);
	}
}

function mapStateToProps(state) {
	return { player: state.player, coords: state.coords, bombs: state.bombs };
}

export default connect(
	mapStateToProps,
	{}
)(Sergeant);
