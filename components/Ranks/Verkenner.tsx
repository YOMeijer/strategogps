import * as React from "react";
import * as _ from "lodash";
import { connect } from "react-redux";
import { AppStore } from "../../reducers";
import { HQ } from "../../reducers/HQsReducer";
import { distance, indexOfMin, bearing } from "../../shared/shared.functions";
import Dial from "../Dial";
import { PlayerState } from "../../reducers/PlayerReducer";
import InactiveDial from "../InactiveDial";
import { GameState } from "../../reducers/GameReducer";

type ThisState = {
	hqTarget: HQ;
	distance: number;
	bearing: number;
	relativeBearing: number;
};

type ThisProps = {
	location: string;
	player: PlayerState;
	hqs: HQ[];
	coords: Coordinates;
	game: GameState;
};

class Verkenner extends React.Component<ThisProps, ThisState> {
	componentWillMount() {
		this.setState({});
	}

	componentWillReceiveProps(nextProps: ThisProps) {
		if (nextProps.coords && nextProps.hqs) {
			console.log(nextProps, "verkenner");
			const distanceArray = _.map(nextProps.hqs, hq => {
				return parseInt(distance(hq.lat, hq.long, nextProps.coords.latitude, nextProps.coords.longitude));
			});

			if (distanceArray.length > 0) {
				console.log(distanceArray);
				console.log(indexOfMin(distanceArray));
				const target = nextProps.hqs[indexOfMin(distanceArray)];
				this.setState({
					hqTarget: target,
					distance: parseInt(distance(nextProps.coords.latitude, nextProps.coords.longitude, target.lat, target.long)),
					relativeBearing:
						parseInt(
							bearing(nextProps.coords.latitude, nextProps.coords.longitude, target.lat, target.long).toFixed()
						) - nextProps.coords.heading,
					bearing: parseInt(
						bearing(nextProps.coords.latitude, nextProps.coords.longitude, target.lat, target.long).toFixed()
					)
				});
			} else {
				this.setState({ hqTarget: null });
			}
		}
	}

	render() {
		return (
			<div>
				{this.state.hqTarget ? (
					<Dial
						name={this.props.player.name}
						distance={this.state.distance}
						relativeBearing={this.state.relativeBearing}
						bearing={this.state.bearing}
						accuracy={this.props.coords.accuracy}
						speed={this.props.coords.speed}
						rank={this.props.player.rank}
						contested={this.props.game.hqContested}
						team={this.props.player.team}
					/>
				) : (
					<InactiveDial
						name={this.props.player.name}
						accuracy={this.props.coords ? this.props.coords.accuracy : 999}
						rank={this.props.player.rank}
						message={"Geen vlaggen gevonden..."}
					/>
				)}
			</div>
		);
	}
}

function mapStateToProps(state: AppStore) {
	return {
		hqs: state.hqs,
		coords: state.coords,
		player: state.player,
		game: state.game
	};
}

export default connect(
	mapStateToProps,
	{}
)(Verkenner);
