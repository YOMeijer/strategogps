import * as React from "react";
import _ from "lodash";

type ThisState = {
	boo: string;
};

type ThisProps = {
	onMarkHq: Function;
};

export default class MarkHQ extends React.Component<ThisProps, ThisState> {
	componentWillMount() {}

	componentWillReceiveProps(nextProps) {}

	markHQ() {
		this.props.onMarkHq();
	}

	render() {
		return (
			<div>
				<button onClick={() => this.markHQ()}>Nieuwe vlag plaatsen</button>
			</div>
		);
	}
}
