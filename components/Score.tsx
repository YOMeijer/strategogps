import * as React from "react";
import * as _ from "lodash";
import { connect } from "react-redux";
import { GameState } from "../reducers/GameReducer";
import { AppStore } from "../reducers";

type ThisState = {};

type ThisProps = {
	game: GameState;
};

class Score extends React.Component<ThisProps, ThisState> {
	componentWillMount() {}

	componentWillReceiveProps(nextProps: ThisProps) {}

	render() {
		return (
			<div>
				{this.props.game && this.props.game.score ? (
					<div className="score-bord">
						<div className="score-team1">{this.props.game.score.team1}</div>
						<div className="divider" />
						<div className="score-team2">{this.props.game.score.team2}</div>
					</div>
				) : null}
			</div>
		);
	}
}

function mapStateToProps(state: AppStore) {
	return { game: state.game };
}

export default connect(
	mapStateToProps,
	{}
)(Score);
