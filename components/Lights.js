import React, { Component } from "react";
import _ from "lodash";
import { connect } from "react-redux";
import { getData, getMotion } from "../actions/Actions";
import {
  getController,
  getYeelight,
  getSonosCurrentTrack,
  getSonosPlayback
} from "../actions/ControllerActions";
import { controller } from "../Firebase.ts";
import LightToggle from "../components/LightToggle";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Sonos from "../components/Sonos";

var Trianglify = require("trianglify");

class Lights extends Component {
  constructor() {
    super();

    this.state = { v: true };
  }

  componentWillMount() {
    this.props.getYeelight();
  }

  componentDidMount() {
    setInterval(() => {
      const height = this.divElement.clientHeight;
      const width = this.divElement.clientWidth;
      const colors = [
        ["#262626", "#4CAFE8", "#FFFFFF"],
        ["#4CAFE8", "#FFFFFF", "#262626"]
      ];

      this.setState({
        triangle: Trianglify({
          seed: 1,
          width: width,
          height: height,
          x_colors: this.state.v ? colors[0] : colors[1],
          cell_size: 31,
          variance: 0.5
        }).svg(),
        v: !this.state.v
      });
    }, 2000);
  }

  getBackground() {
    return _.map(this.state.triangle.childNodes, (node, i) => {
      return React.createElement("path", {
        ...this.getAttrs(node),
        key: i
      });
    });
  }

  getAttrs(node) {
    return {
      d: node.getAttribute("d"),
      fill: node.getAttribute("fill"),
      strokeWidth: node.getAttribute("stroke-width"),
      stroke: node.getAttribute("stroke")
    };
  }

  componentWillReceiveProps(nextProps) {}

  renderLightToggles() {
    return _.map(this.props.yeelight, (light, key) => {
      return <LightToggle key={key} light={light} name={key} />;
    });
  }

  render() {
    const svg = React.createElement("svg", { height: 200, width: 200 });
    return (
      <div
        className="light-toggles"
        ref={divElement => (this.divElement = divElement)}
      >
        {this.state.triangle ? (
          <svg className="back-svg" height="100%" width="100%">
            {this.getBackground()}
          </svg>
        ) : null}

        {this.renderLightToggles()}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    yeelight: state.yeelight
  };
}

export default connect(
  mapStateToProps,
  { getYeelight }
)(Lights);
